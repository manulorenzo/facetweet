//
//  PRPAppDelegate.h
//  FirstProjectTwitter
//
//  Created by manu on 31/12/2012.
//  Copyright (c) 2012 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRPViewController;

@interface PRPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) PRPViewController *viewController;

@end
