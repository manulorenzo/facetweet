//
//  PRPViewController.m
//  FirstProjectTwitter
//
//  Created by manu on 31/12/2012.
//  Copyright (c) 2012 Manuel. All rights reserved.
//

#import "PRPViewController.h"
#import "Social/Social.h"

@interface PRPViewController ()
-(void) reloadTweets;
-(void) handleTwitterData: (NSData*) data
              urlResponse: (NSHTTPURLResponse*) urlResponse
                    error: (NSError*) error;
@property (nonatomic, strong) IBOutlet UITextView *twitterTextView;
@end

@implementation PRPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) handleTwitterData:(NSData *)data
               urlResponse:(NSHTTPURLResponse *)urlResponse
                     error:(NSError *)error {
    NSError *jsonError = nil;
    NSJSONSerialization *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
    NSLog(@"jsonResponse: %@", jsonResponse);
    if (!jsonError && [jsonResponse isKindOfClass:[NSArray class]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *tweets = (NSArray *)jsonResponse;
            for (NSDictionary *tweetDict in tweets) {
                NSString *tweetText = [NSString stringWithFormat:@"%@ (%@)", [tweetDict valueForKey:@"text"], [tweetDict valueForKey:@"created_at"]];
                self.twitterTextView.text = [NSString stringWithFormat:@"%@%@\n\n", self.twitterTextView.text, tweetText];
            }
        });
    }
}

-(void)reloadTweets {
    NSURL *twitterAPIURL = [NSURL URLWithString:@"http://api.twitter.com/1/statuses/user_timeline.json"];
    NSDictionary *twitterParams = @ { @"screen_name":@"noloman",};
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:twitterAPIURL parameters:twitterParams];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        [self handleTwitterData:responseData
                    urlResponse:urlResponse
                          error:error];
    }];
}

- (IBAction)handleTweetButtonTapped:(id)sender
{
    NSLog(@"Button tapped");
    if ([SLComposeViewController isAvailableForServiceType: SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSLog(@"%@", [[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter] class]);
        [tweetVC setInitialText:NSLocalizedString(@"TITLE", nil)];
        tweetVC.completionHandler = ^(SLComposeViewControllerResult result) {
            if (result == SLComposeViewControllerResultDone) {
                [self dismissViewControllerAnimated:YES completion:NULL];
                [self reloadTweets];
            }
        };
        [self presentViewController:tweetVC animated:YES completion:NULL];
    } else {
        NSLog(@"Can't send tweet");
    }
}

- (IBAction)showMyTweetsTapped:(id)sender
{
    [self reloadTweets];
}

- (IBAction)handleFacebookButtonTapped:(id)sender {
    NSLog(@"Button tapped");
    if ([SLComposeViewController isAvailableForServiceType: SLServiceTypeFacebook])
    {
        SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebook setInitialText:@"Just finished!"];
        [self presentViewController:facebook animated:YES completion:NULL];
    }else {
        NSLog(@"Can't send tweet");
    }
}
@end
