//
//  main.m
//  FirstProjectTwitter
//
//  Created by manu on 31/12/2012.
//  Copyright (c) 2012 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PRPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PRPAppDelegate class]));
    }
}
