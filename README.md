Simple iOS 6 application to tweet and post to facebook simple strings.

The app itself is meant to help the developer familiarize with the new iOS 6 Social Frameworks for Twitter and Facebook.

Author: Manuel Lorenzo (http://www.manulorenzo.net)